    <?php
        include_once __DIR__."/../../Model/Kategori.php";
        $listKategori = Kategori::getAll();
    ?>

    <h1>Tambah Produk</h1>
    <form action="/view/produk/prosesTambah.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="">Kode</label>
            <input type="text" class="form-control" name="kode" required />
        </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" class="form-control" name="nama" required />
        </div>
        <div class="form-group">
            <label for="">Harga</label>
            <input type="number" class="form-control" min='1' name="harga" required />
        </div>
        <div class="form-group">
            <label for="">Stok</label>
            <input type="number" class="form-control" min="0" name="stok" required />
        </div>
        <div class="form-group">
            <label for="">Gambar</label>
            <input accept=".jpg,.png" type="file" class="form-control" name="gambar" id="">
        </div>

        <div class="form-group">
            <label for="">Kategori</label>
            <select name="id_kategori" class="form-control" id="">
                <?php foreach ($listKategori as $kategori) { ?>
                    <option value="<?= $kategori->id ?>">
                        <?= $kategori->nama ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <a class="btn btn-info" href="/index.php">Kembali</a>
        <button class="btn btn-success" type="submit">Simpan Data</button>
    </form>