<?php
include_once __DIR__.'/../../Model/Produk.php';
include_once __DIR__."/../../Config/role_validation.php";

#ambil semua paramater
$id = $_REQUEST['id'];
$kode = $_REQUEST['kode'];
$nama = $_REQUEST['nama'];
$harga = $_REQUEST['harga'];
$stok = $_REQUEST['stok'];
$id_kategori = $_REQUEST['id_kategori'];
#ambil data produk by primary key
$produk = Produk::getByPrimaryKey($id);
if($produk === null){
    echo "Data tidak ditemukan <br> <a href='/index.php'>Kembali</a>";
    exit;
}
#update data produk
$produk->kode = $kode;
$produk->nama = $nama;
$produk->harga = $harga;
$produk->stok = $stok;
$produk->id_kategori = $id_kategori;
#jalankan proses update
$produk->update();
header('Location: /index.php');