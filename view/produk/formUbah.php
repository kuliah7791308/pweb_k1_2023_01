<?php
include_once __DIR__ . '/../../Model/Produk.php';
include_once __DIR__ . '/../../Model/Kategori.php';

if (isset($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
    $produk = Produk::getByPrimaryKey($id);
} else {
    header('Location: /index.php');
}
$listKategori = Kategori::getAll();
?>
<h1>Edit data Produk</h1>
<form action="/view/produk/prosesUbah.php" method="post">
    <div class="form-group">
        <label for="">Kode</label>
        <input type="text" name="kode" value="<?= $produk->kode ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label for="">Nama</label>
        <input type="text" name="nama" value="<?= $produk->nama ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label for="">Harga</label>
        <input type="number" min='1' name="harga" value="<?= $produk->harga ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label for="">Stok</label>
        <input type="number" min='0' name="stok" value="<?= $produk->stok ?>" class="form-control" />
    </div>
    <div class="form-group">
        <label for="">Kategori</label>
        <select name="id_kategori" class="form-control" id="">
            <?php foreach ($listKategori as $kategori) { ?>
            <option <?= $kategori->id == $produk->id_kategori ? 'selected' : '' ?> value="<?= $kategori->id ?>">
                <?= $kategori->nama ?>
            </option>
            <?php } ?>
        </select>
    </div>
    <input type="hidden" name="id" value="<?= $produk->id ?>">
    <a class="btn btn-info" href="/index.php">Kembali</a>
    <button class="btn btn-success" type="submit">Simpan</button>
    <button class="btn btn-dark" type="reset">Reset</button>
</form>