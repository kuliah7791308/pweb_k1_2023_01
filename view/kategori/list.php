<?php
include_once __DIR__ . "/../../Model/Kategori.php";
$listKategori  = Kategori::getAll();
?>

<h1>Data Kategori</h1>
<table class="table table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($listKategori as $kategori) {
        ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $kategori->nama ?></td>
                <td>
                    <a class="btn btn-sm btn-dark" href="#">
                        <i class="fa fa-edit"></i>
                    </a>
                    <button class="btn btn-sm btn-danger btn-hapus" data-id='<?= $kategori->id ?>' type="button">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script>
    $(function() {
        $('.btn-hapus').click(function() {
            let idKategori = $(this).data('id');
            Swal.fire({
                title: 'Yakin Hapus ?',
                text: "Data tidak akan dapat kembali setelah dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    location.href = '/view/kategori/prosesHapus.php?id=' + idKategori;
                }
            })
        });
    });
</script>