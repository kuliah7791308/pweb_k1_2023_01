create database pweb_k1_2023_01;
use pweb_k1_2023_01;

create table produk(
    id int not null primary key auto_increment,
    kode varchar (10) unique not null,
    nama varchar (50) unique not null ,
    harga decimal(10,2),
    stok int default '0'
);

insert into produk values (null,'P001','Gelas Kopi',5000,10);
insert into produk values (null,'P002','Kabel HDMI',150000,100);


alter table produk
add deleted_at datetime default null;


create table kategori(
    id int not null primary key auto_increment,
    nama varchar(200) not null
);

alter table produk add id_kategori int;

insert into kategori(nama) values
 ('Makanan'),('Minuman'),('Elektronik');

insert into kategori(nama) values
 ('Pakaian'),('ATK');
 
 alter table produk add gambar varchar(200) default null;
 
 drop table user;
create table user(
    id int not null primary key auto_increment,
    username varchar(50) not null unique,
    password varchar(100) not null,
    token varchar(100),
    role enum('superadmin','admin') default 'admin'
);
insert into user(username,password,role) 
values ('superadmin','$2y$10$nKPDlFKuaE1Dmoy6vsNGMOeYXT4t04gfwLGuzhKHIcyzW./RqwajK','superadmin');


insert into user(username,password,role) 
values ('joko','$2y$10$Ylapg8gTqpH9wzTcqkhVKecWLW7ZX9f/6zeGHXa0jbIzxPpYWjoF2','admin');
