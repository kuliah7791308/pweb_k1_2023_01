<?php
class Koneksi
{
    private $host = "localhost";
    private $port = "3306";
    private $username = "root";
    private $password = "";
    private $databaseName = "pweb_k1_2023_01";
    public $koneksi;

    public function __construct()
    {
        $this->koneksi = mysqli_connect(
            $this->host,
            $this->username,
            $this->password,
            $this->databaseName,
            $this->port
        );
    }
}
